﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Kart.User32;

namespace Kart
{
    class ProcessManager
    {
        private const string processName = "KartRider";
        
        public Process GetProcess()
        {
            Process[] ps = Process.GetProcessesByName(processName);

            if (ps.Length < 1)
                return null;

            return ps[0];
        }

        public void ShowWindow(Process p)
        {
            SetForegroundWindow(p.MainWindowHandle);
            User32.ShowWindow(p.MainWindowHandle, SW_RESTORE);
            Thread.Sleep(100);
        }

        public Rect Rect(Process p)
        {
            Rect rect = new Rect();
            ShowWindow(p);
            GetWindowRect(p.MainWindowHandle, ref rect);
            return rect;
        }
    }
}
