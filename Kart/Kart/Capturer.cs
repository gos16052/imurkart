﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kart
{
    public class Capturer
    {
        private readonly string processName;
        private Process p;
        private User32.Rect rect;
        WebController wc;
        
        public Capturer(string pname)
        {
            processName = pname;
            rect = new User32.Rect();
        }

        private void SetProcess()
        {
            Process[] ps = Process.GetProcessesByName(processName);

            if (ps.Length < 1)
            {
                wc = new WebController();
                wc.Start();
            }
            p = ps[0];
        }

        public void ShowWindow()
        {
            User32.SetForegroundWindow(p.MainWindowHandle);
            User32.ShowWindow(p.MainWindowHandle, User32.SW_RESTORE);
            Thread.Sleep(100);
        }

        public Bitmap Capture()
        {
            SetProcess();
            ShowWindow();
            User32.GetWindowRect(p.MainWindowHandle, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(rect.left, rect.top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
            return bmp;
        }
    }
    
    /*
    public class User32
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct Rect
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }
        public const int SW_RESTORE = 9;

        [DllImport("user32.dll")]
        public static extern IntPtr ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);
    }
    */
}
