﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Management;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.IO.Ports;
using static Kart.User32;
using System.Windows.Forms;

namespace Kart
{
    public class Program
    {
        public USBMouse mouse;
        ProcessManager pm;
        Macro m;
        WebController wc = null;
        public static Program self;
        bool IsRoom = false;

        public Program()
        {
            if (File.Exists("idpw.txt"))
                wc = new WebController();

            mouse = new USBMouse();
            pm = new ProcessManager();
            m = new Macro();
        }

        Point ToAbsolute(Rect r, Point p) => new Point(r.left + p.X, r.top + p.Y);
        public void ClickAction(Rect rect, string act, string msg)
        {
            Console.WriteLine(msg);
            mouse.Click(ToAbsolute(rect, m.Get(act)));
            Thread.Sleep(1000);
        }

        Process GetOrRunProcess()
        {
            Process p;
            p = pm.GetProcess();

            if (p == null)
            {
                IsRoom = false;
                Console.WriteLine("카트프로세스가 없으므로 카트를 실행");
                if (wc == null)
                {
                    Console.WriteLine("idpw.txt가 없어서 카트가 팅겨도 실행을 못해여 ㅅㄱ");
                    Application.Exit();
                }
                wc.Start();
                p = pm.GetProcess();
                Rect r = pm.Rect(p);
                ClickAction(r, "mission", "미션출석체크닫기");
                CreateRoom(r);
            }
            
            return p;
        }

        void Run()
        {
            Rect r;
            
            r = pm.Rect(GetOrRunProcess());
            ClickAction(r, "server", "서버끊김확인버튼");
            r = pm.Rect(GetOrRunProcess()); // 카트가 꺼진다면 selenium을 통해 카트가 실행됨 -->실행될때 처리필요
            ClickAction(r, "showtime", "쇼타임닫기");
            ClickAction(r, "expired", "템사용기간만료메시지 버튼");
            ClickAction(r, "showtime", "쇼타임닫기");
            ClickAction(r, "expired", "템사용기간만료메시지 버튼");
            ClickAction(r, "messenger", "메신저버튼 클릭");
            ClickAction(r, "messenger", "메신저버튼 클릭 (닫아짐)");
            ClickAction(r, "closenoti", "노티 닫기");
            ClickAction(r, "noti", "노티버튼"); 
            ClickAction(r, "clicknoti", "노티메시지열기");
            ClickAction(r, "receive", "템받기");
            ClickAction(r, "confirm", "받기확인");
            ClickAction(r, "closenoti", "노티메시지닫기");
            Console.WriteLine(DateTime.Now.ToString("hh:mm:ss") + "\n");
        }

        void CreateRoom(Rect r)
        {
            if (IsRoom)
                return;
            IsRoom = true;
            Console.WriteLine("방만들기");
            ClickAction(r, "multiplay", "멀티플레이버튼");
            ClickAction(r, "speed", "스피드전");
            ClickAction(r, "veryfast", "매빠");
            ClickAction(r, "createroom", "방만들기");
            ClickAction(r, "private", "비공개방");
            ClickAction(r, "createroomconfirm", "방만들기확인");
            ClickAction(r, "closeone", "한칸닫기");
        }
        void Init() => CreateRoom(pm.Rect(GetOrRunProcess()));

        static void Main(string[] args)
        {
            Program p = new Program();
            self = p;

            p.Init();
            while (true)
            {
                p.Run();
                Thread.Sleep(300 * 1000); // 5 minutes
            }
        }

    }

}
