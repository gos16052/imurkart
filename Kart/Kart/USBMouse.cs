﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.IO;
using System.Management;
using LibUsbDotNet.DeviceNotify;
using LibUsbDotNet;
using LibUsbDotNet.Main;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;

namespace Kart
{
    public class USBMouse
    {
        private const string DEVICEFILE = "device.txt";
        private const string PNPID = "PNPDeviceID";
        private const string PORT = "DeviceID";

        private SerialPort sp;
        
        public USBMouse()
        {
            Console.WriteLine("아두이노 연결 시도~");
            TryConnect();
            
            while (sp == null || !sp.IsOpen)
            {
                Console.WriteLine("연결실패.. " + sp.PortName + " 재시도");
                TryConnect();
            }
            Console.WriteLine("연결성공 " + sp.PortName);

            sp.DataReceived += (sender, e) =>
            {
                //SerialPort port = (SerialPort)sender;
                Console.WriteLine(sp.Equals((SerialPort)sender));
                string data = sp.ReadExisting();
                Console.WriteLine(data);
            };
        }

        public void TryConnect()
        {
            try
            {
                string portname = Port();
                sp = new SerialPort();
                sp.PortName = portname;
                sp.RtsEnable = true;
                sp.DtrEnable = true;
                sp.BaudRate = 9600;
                sp.Open();
            } catch (Exception) {}
        }

        private string RegisterDevice()
        {
            Console.WriteLine("최초연결과정, 아두이노를 연결해제 한 뒤 엔터 쳐주십쇼");
            Console.ReadLine();
            Console.WriteLine("ㄱㄷ");

            ManagementObjectCollection moc = GetPortInfo();
            string[] previds = new string[moc.Count];
            int i = 0;
            foreach (ManagementObject m in moc)
            {
                previds[i] = (string) m[PNPID];
                i++;
            }

            Console.WriteLine("아두이노 연결하고 엔터 쳐주십쇼");
            Console.ReadLine();
            Console.WriteLine("ㄱㄷ");

            moc = GetPortInfo();
            foreach (ManagementObject m in moc)
            {
                if (Array.IndexOf(previds, (string) m[PNPID]) == -1)
                {
                    string devid = ToDeviceID(m[PNPID]);
                    using (StreamWriter sw = new StreamWriter(DEVICEFILE))
                        sw.Write(devid);
                    
                    Console.WriteLine("PNPDeviceID is " + devid);
                    return devid;
                }
            }

            throw new Exception("Not Found DeviceID");
        }

        private string ToDeviceID(object pnpid) => pnpid.ToString().Substring(0, 12);
        private string GetPort()
        {
            ManagementObjectCollection moc = GetPortInfo();
            string id;

            using (StreamReader sr = new StreamReader(DEVICEFILE))
                id = sr.ReadLine();

            Console.WriteLine("내 디바이스아이디는.. " + id);

            foreach (ManagementObject m in moc) // iterable로 만드는 시간 오래걸림
            {
                string devid = ToDeviceID(m[PNPID]);
                Console.WriteLine(devid);
                if (id.Equals(devid))
                    return (string)m[PORT];
            }

            throw new Exception("Not Found DeviceID");
        }
        
        private string Port()
        {
            if (!File.Exists(DEVICEFILE))
                return RegisterDevice();
            else
                return GetPort();
        }

        private void ShowAllDeviceInfo()
        {
            using (var searcher = new ManagementObjectSearcher
                    ("SELECT PNPDeviceID, DeviceID " +
                    "FROM WIN32_SerialPort " +
                    "WHERE PNPDeviceID LIKE \"USB%\""))
            {
                Console.WriteLine("GetPortNames");
                string[] portnames = SerialPort.GetPortNames();
                Console.WriteLine("Search");
                ManagementObjectCollection moc = searcher.Get();
                foreach (ManagementObject m in moc)
                {
                    foreach (PropertyData pd in m.Properties)
                    {
                        Console.Write(pd.Name + " - ");
                        if (pd.Name.Equals("PowerManagementCapabilities"))
                        {
                            ushort[] v = (ushort[])m[pd.Name];
                            foreach (ushort i in v)
                                Console.Write(i + " ");
                            Console.WriteLine();
                        }
                        else
                            Console.WriteLine(m[pd.Name]);
                    }
                    Console.WriteLine();
                }
            }
        }
        public void Close() => sp.Close();
        public void Click(Point p)
        {
            MoveTo(p);
            Click();
        }
        public void SendMsg(string s)
        {
            Console.WriteLine(s);
            sp.Write(s);
        }
        public string GenMsg(char cmd, int x, int y) => cmd + "|" + x + "|" + y + "*";
        //public void Click() => SendMsg(GenMsg('c', 0, 0));
        public void Click() => sp.Write("c");
        public void Move(int dx, int dy)
        {
            int ddx, ddy;
            bool xPositive = dx > 0;
            bool yPositive = dy > 0;
            int absX = Math.Abs(dx);
            int absY = Math.Abs(dy);
            
            while (absX != 0 || absY != 0)
            {
                ddx = absX > 127 ? 127 : absX;
                if (!xPositive) ddx = -ddx;
                ddy = absY > 127 ? 127 : absY;
                if (!yPositive) ddy = -ddy;

                SendMsg(GenMsg('m', ddx, ddy));
                absX -= Math.Abs(ddx);
                absY -= Math.Abs(ddy);
            }
            Thread.Sleep(10);
        }

        public void MoveTo(Point pos)
        {
            Cursor.Position = pos;
            /*
            Point cur = Cursor.Position;
            Console.WriteLine(cur);
            int dx = pos.X - cur.X;
            int dy = pos.Y - cur.Y;
            Move(dx, dy);
            */
        }
        private static ManagementObjectCollection GetPortInfo() => (new ManagementObjectSearcher("SELECT PNPDeviceID, DeviceID FROM WIN32_SerialPort WHERE PNPDeviceID LIKE \"USB%\"")).Get();
    }
}

/*
 * 확인버튼이떠있는가 검사
 * 확인이 떠있으면 -> 눌러
 * 프로세스체크
 * 프로세스가 죽었으면 켜
 * 
 */