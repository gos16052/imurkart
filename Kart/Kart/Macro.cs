﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kart
{
    public class Macro
    {
        const string MACROFILE = "macro.txt";

        Dictionary<string, Point> macroDict = new Dictionary<string, Point>();
        
        public Macro()
        {
            string line;
            string[] slice;
            Point p;

            using (StreamReader sr = new StreamReader(MACROFILE))
            {
                do
                {
                    line = sr.ReadLine();
                    if (line == null || line.Length == 0) break;
                    slice = line.Trim().Split(' ');
                    if (slice.Length == 0) break;

                    p = new Point(int.Parse(slice[1]), int.Parse(slice[2]));
                    macroDict.Add(slice[0], p);
                } while (true);
            }
        }

        public Point Get(string key) => macroDict[key];
    }
}
