﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using System.Security;
using OpenQA.Selenium.Interactions;
using System.Diagnostics;

namespace Kart
{
    public class WebController
    {
        private const string LOGIN_URL= "https://nxlogin.nexon.com/common/login.aspx?redirect=http%3a%2f%2fkart.nexon.com%2fMain%2fIndex.aspx";

        private const string KART_URL = "http://kart.nexon.com/Main/Index.aspx";
        private const string KART_START_BTN = "btn_gamestart";
        private const string KART_LOGIN_BTN = "btnLogin";
        private const string LOGIN_ID = "txtNexonID";
        private const string LOGIN_PW = "txtPWD";

        private const string FILE_IDPW = "idpw.txt";

        private IWebDriver driver = null;
        private string ID;
        private string PW;
        
        
        public WebController()
        {
            using (StreamReader sr = new StreamReader("idpw.txt"))
            {
                ID = sr.ReadLine().Trim();
                PW = sr.ReadLine().Trim();
            }
        }
        
        public void Login()
        {
            IWebElement e;
            if (driver == null)
                driver = new ChromeDriver();

            driver.Url = LOGIN_URL;
            
            e = driver.FindElement(By.Id(LOGIN_ID));
            e.SendKeys(ID);
            Thread.Sleep(1000);
            e = driver.FindElement(By.Id(LOGIN_PW));
            e.SendKeys(PW);
            Thread.Sleep(1000);
            e = driver.FindElement(By.Id(KART_LOGIN_BTN));
            e.Click();

            Console.WriteLine("Login\n");
        }

        public void Start()
        {
            do
            {
                Login();
                if (!driver.Url.Equals(KART_URL))
                    driver.Url = KART_URL;
            } while (driver.FindElements(By.ClassName("user_name")).Count == 0);
               


            Thread.Sleep(1000);
            IWebElement e = driver.FindElement(By.ClassName(KART_START_BTN));
            Thread.Sleep(1000);
            e.Click();
            Thread.Sleep(1000);
            
            IWindow w = driver.Manage().Window;
            int x = w.Position.X + w.Size.Width / 2;
            int y = w.Position.Y + 180;
            Program.self.mouse.Click(new System.Drawing.Point(x, y));
            Console.WriteLine("Start & sleep 2 minute\n");
            Thread.Sleep(120 * 1000);
            driver.Close();
            driver.Dispose();
            driver = null;
        }
        
    }

    public static class SS
    {
        public static void Append(this SecureString s, string str)
        {
            foreach (char ch in str)
                s.AppendChar(ch);
        }
    }
}
