﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kart
{
    public class ImgProcess
    {
        private const string PATH_IMG = "images/";

        private Capturer capturer;

        public ImgProcess()
        {
            capturer = new Capturer("KartRider");
            Console.WriteLine(Cursor.Position);
            Bitmap bmp = capturer.Capture();
            
            bmp.Save("test.png", ImageFormat.Png);
        }
        
        
        public bool IsEquivalent(Bitmap small, Bitmap large, int sx, int sy)
        {
            for (int i = 0; i < small.Width; i++)
            {
                for (int j = 0; j < small.Height; j++)
                {
                    if (small.GetPixel(i, j) != large.GetPixel(i + sx, j + sy))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void FindPosition(Bitmap all, Bitmap target)
        {
            for (int i = 0; i < all.Width; i++)
            {
                for (int j = 0; j < all.Height; j++)
                {
                    if (all.GetPixel(i, j) == target.GetPixel(0, 0)
                        && IsEquivalent(target, all, i, j))
                    {
                        Console.WriteLine(string.Format("%d %d", i, j));
                    }
                }
            }
        }
    }
}
